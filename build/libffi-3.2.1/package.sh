##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the FFI library.
##
## Author:
##
##     Evan Green 27-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"

##
## Remove the libtool garbage.
##

rm "$BUILD_DIRECTORY"/usr/lib/*.la
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libffi
Depends: libgcc
Priority: optional
Version: 3.2.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://sourceware.org/pub/libffi/libffi-3.2.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Foreign Function Interface library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

