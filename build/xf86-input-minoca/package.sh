##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the xf86-input-minoca library.
##
## Author:
##
##     Evan Green 6-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/lib/xorg/modules/drivers"
cp -pv "$BUILD_DIRECTORY/minoca_drv.so" \
 "$PACKAGE_DIRECTORY/usr/lib/xorg/modules/drivers"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: xf86-input-minoca
Priority: optional
Version: 1.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://gitlab.com/minoca/third-party/build/xf86-input-minoca
Installed-Size: $INSTALLED_SIZE
Description: Minoca OS X input driver.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

