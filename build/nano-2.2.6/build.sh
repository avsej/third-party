##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the nano package.
##
## Author:
##
##     Evan Green 11-Jul-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
READLINE=libreadline_6.3

##
## Export some variables needed to build correctly on Windows.
##

if test "x$BUILD_OS" = "xwin32"; then
    CPPFLAGS="$CPPFLAGS -I$OUTPUT_DIRECTORY/include"
    LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/lib"
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$NCURSES"
    extract_dependency "$READLINE"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --prefix="/usr" \
                                     --program-prefix= \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

