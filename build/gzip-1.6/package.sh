##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the gzip utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gzip
Priority: optional
Version: 1.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/gzip/gzip-1.6.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU gzip utilities for working with with compressed files.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

