##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the dmake utility.
##
## Author:
##
##     Evan Green 16-May-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv $BUILD_DIRECTORY/usr/* "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: dmake
Depends: libgcc
Priority: optional
Version: 4.12.2.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://github.com/mohawk2/dmake/archive/DMAKE_4_12_2_2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: make alternative
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

