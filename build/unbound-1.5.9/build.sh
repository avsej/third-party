##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the unbound package.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14
GETTEXT=gettext_0.19.8.1
LIBOPENSSL=libopenssl_1.0.2h
LIBEVENT=libevent_2.0.22
EXPAT=expat_2.1.0

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    extract_dependency "$GETTEXT"
    extract_dependency "$LIBOPENSSL"
    extract_dependency "$LIBEVENT"
    extract_dependency "$EXPAT"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export UNAME=Minoca
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --disable-rpath \
                                     --with-libevent="$DEPENDROOT/usr" \
                                     --with-ssl="$DEPENDROOT/usr" \
                                     --with-libexpat="$DEPENDROOT/usr" \
                                     --with-pthreads \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --with-pidfile=/var/run/unbound.pid \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

