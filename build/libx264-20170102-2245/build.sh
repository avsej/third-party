##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the x264 video encoding library package.
##
## Author:
##
##     Chris Stevens 18-Jan-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"

    ##
    ## The assembly seems to use the rbit instruction, which is not available
    ## in ARM mode.
    ##

    [ "$ARCH" = "armv6" ] && DISABLE_ASM=--disable-asm
    sh ${SOURCE_DIRECTORY}/configure --host="$TARGET" \
                                     --enable-shared \
                                     --prefix="/usr" \
                                     $DISABLE_ASM \
                                     --extra-cflags="$CFLAGS" \
                                     --extra-ldflags="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

