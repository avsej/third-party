##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the LLVM package.
##
## Author:
##
##     Chris Stevens 18-Jul-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency cmake_3.5.2

    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    export CXX="$TARGET-g++"
    export CXXFLAGS="$CFLAGS -std=c++11"

    ##
    ## -lminocaos is needed in CMAKE_STANDARD_LIBRARIES because an LLVM
    ## module uses __clear_cache.
    ##

    cmake -D CMAKE_INSTALL_PREFIX=/usr \
          -D CMAKE_BUILD_TYPE=Release \
          -D CMAKE_C_FLAGS="$CFLAGS" \
          -D CMAKE_CXX_FLAGS="$CXXFLAGS" \
          -D CMAKE_EXE_LINKER_FLAGS="$LDFLAGS" \
          -D CMAKE_MODULE_LINKER_FLAGS="$LDFLAGS" \
          -D CMAKE_CXX_STANDARD_LIBRARIES="-lminocaos" \
          -D LLVM_BUILD_LLVM_DYLIB=ON \
          -D LLVM_LINK_LLVM_DYLIB=ON \
          -D LLVM_TARGETS_TO_BUILD="host" \
          -D LLVM_INSTALL_UTILS=ON \
          ${SOURCE_DIRECTORY}

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

