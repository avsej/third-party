##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the LLVM packages.
##
## Author:
##
##     Chris Stevens 18-July-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: llvm
Priority: optional
Version: 3.9.1
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://llvm.org/releases/3.9.1/llvm-3.9.1.src.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: The LLVM compiler infrastructure project.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

