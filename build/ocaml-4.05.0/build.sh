##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the OCAML package.
##
## Author:
##
##     Chris Stevens 21-Jul-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  configure)

    ##
    ## Ocaml tests for X.h, but only uses X11.h during configure. X.h is
    ## actually in xproto, not libX11.
    ##

    extract_dependency libx11_1.6.5
    extract_dependency xproto_7.0.31
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8

    export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$LD_LIBRARY_PATH"
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure --prefix "/usr" \
                                     --host "$TARGET" \
                                     --target "$TARGET" \
                                     -cc "$CC" \
                                     --x11include "$DEPENDROOT/usr/include" \
                                     --x11lib "$DEPENDROOT/usr/lib"

    ;;

  build)

    ##
    ## Ocamldoc fails in the opt.opt step with parallel make.
    ##

    $MAKE $PARALLEL_MAKE world
    $MAKE $PARALLEL_MAKE opt
    $MAKE opt.opt
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

