################################################################################
#
#   Copyright (c) 2016 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       ninja-1.7.1
#
#   Abstract:
#
#       This makefile is responsible for building the ninja build utility.
#
#   Author:
#
#       Evan Green 20-Jul-2016
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := ninja-1.7.1
PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig

TOOLBUILDROOT := $(OBJROOT)/$(PACKAGE).tool
BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

.PHONY: all clean recreate-diff package

all: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package

tools: | $(TOOLBINROOT) $(TOOLBUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" build-tools

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -Nru3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(TOOLBUILDROOT) $(BUILDROOT) $(BINROOT):
	mkdir -p $@

