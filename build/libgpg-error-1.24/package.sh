##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the gpg-error library.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY"/usr/share/man
rm -rf "$BUILD_DIRECTORY"/usr/share/info

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libgpg-error
Depends: libiconv, gettext, libgcc
Priority: optional
Version: 1.24
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.24.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GNUPG error library
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

