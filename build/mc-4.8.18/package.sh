##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages mc.
##
## Author:
##
##     Vasco Costa 23-Nov-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/libexec" "$PACKAGE_DIRECTORY/libexec"
cp -Rpv "$BUILD_DIRECTORY/usr/share/locale" "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/mc" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: mc
Depends: libgcc, libglib, libncurses, libpcre
Priority: optional
Version: 4.8.18
Architecture: $PACKAGE_ARCH
Maintainer: Vasco Costa <gluon81@gmx.com>
Section: main
Source: http://ftp.midnight-commander.org/mc-4.8.18.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GNU Midnight Commander is a text-mode full-screen file manager.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

