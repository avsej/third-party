##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libevent library.
##
## Author:
##
##     Evan Green 20-Jul-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h

##
## Export some variables needed to build correctly on Windows.
##

if test "x$BUILD_OS" = "xwin32"; then
    export gl_cv_func_memchr_works=yes
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$OPENSSL" || echo "Building without OpenSSL."
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --disable-static \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

