##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the rsync utility.
##
## Author:
##
##     Evan Green 4-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/usr/bin/rsync" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: rsync
Priority: optional
Version: 3.1.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.samba.org/ftp/rsync/src/rsync-3.1.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: rsync remote file sync utility.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

