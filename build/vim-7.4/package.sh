##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the tar utility.
##
## Author:
##
##     Evan Green 2-Nov-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/vim" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: vim
Depends: libncurses
Priority: optional
Version: 7.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://ftp.vim.org/pub/vim/unix/vim-7.4.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: Vi-style editor.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

