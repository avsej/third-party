##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages irssi.
##
## Author:
##
##     Vasco Costa 11-Nov-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr/share/irssi" "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: irssi
Depends: libgcc, libglib, libncurses, libopenssl, libiconv, gettext
Priority: optional
Version: 1.0.0
Architecture: $PACKAGE_ARCH
Maintainer: Vasco Costa <gluon81@gmx.com>
Section: main
Source: https://github.com/irssi/irssi/releases/download/1.0.0/irssi-1.0.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Irssi is a terminal based IRC client for UNIX systems. It also supports SILC and ICB protocols via plugins.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

