##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages autoconf.
##
## Author:
##
##     Evan Green 11-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share/autoconf" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: autoconf
Depends: libgcc
Priority: optional
Version: 2.69
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU autoconfiguration build utility
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

