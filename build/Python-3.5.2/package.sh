##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Python 3.5 core.
##
## Author:
##
##     Evan Green 15-Dec-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rp "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

##
## Remove all the test junk and some other bloat.
##

rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/distutils/tests"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/idlelib"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/lib2to3/tests"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/sqlite3/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/ctypes/test"
rm -rf "$PACKAGE_DIRECTORY/usr/lib/python3.5/test"

##
## 2to3 is provided by the Python2 package.
##

rm -rf "$PACKAGE_DIRECTORY/usr/bin/2to3"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: python3
Depends: libopenssl, libreadline, libncurses, sqlite, expat
Priority: optional
Version: 3.5.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tgz
Installed-Size: $INSTALLED_SIZE
Description: Python is an interpreted scripting language.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

