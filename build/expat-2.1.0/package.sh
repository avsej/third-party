##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the expat library.
##
## Author:
##
##     Evan Green 18-Mar-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f "$BUILD_DIRECTORY"/usr/lib/*.la
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: expat
Depends: libgcc
Priority: optional
Version: 2.1.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://downloads.sourceforge.net/expat/expat-2.1.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The expat library assists in parsing XML.
 functions.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

