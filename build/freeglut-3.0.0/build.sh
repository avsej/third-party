##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the GLUT package.
##
## Author:
##
##     Evan Green 26-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency glproto_1.4.17
    extract_dependency xproto_7.0.31
    extract_dependency kbproto_1.0.7
    extract_dependency xextproto_7.3.0
    extract_dependency libxau_1.0.8
    extract_dependency libx11_1.6.5
    extract_dependency libxext_1.3.3
    extract_dependency libxcb_1.12
    extract_dependency dri2proto_2.8
    extract_dependency libxdamage_1.1.4
    extract_dependency libxfixes_5.0.3
    extract_dependency fixesproto_5.0
    extract_dependency damageproto_1.2.1
    extract_dependency inputproto_2.3.2
    extract_dependency expat_2.1.0
    extract_dependency libxdmcp_1.1.2
    extract_dependency mesa_17.0.0
    extract_dependency libglu_9.0.0
    extract_dependency libxi_1.7.9

    extract_dependency cmake_3.5.2

    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -Wno-unused-const-variable"

    export CXXFLAGS="$CFLAGS"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    cmake -DCMAKE_INSTALL_PREFIX=/usr \
          -DCMAKE_BUILD_TYPE=Release \
          -DFREEGLUT_BUILD_DEMOS=OFF \
          -DFREEGLUT_BUILD_STATIC_LIBS=OFF \
          ${SOURCE_DIRECTORY}

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

