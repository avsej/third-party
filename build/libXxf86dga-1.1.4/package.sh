##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the libXxf86dga library.
##
## Author:
##
##     Evan Green 22-Mar-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm $BUILD_DIRECTORY/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libxxf86dga
Depends: libxau, libxcb, libx11, libxext, libsm, libice, libxt, libxmu, libxpm, libxfixes, libxrender, libz, libfreetype, xtrans, libfontenc, libxi, libxv, libgcc
Priority: optional
Version: 1.1.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.x.org/pub/individual/lib/libXxf86dga-1.1.4.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: Some X library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

