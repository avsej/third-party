##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the GMP package.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'

    ##
    ## Determine whether to build a 32 or 64 bit ABI version. Using uname -m
    ## is not good enough, as the machine might be 64 bits while the
    ## compiler is still only 32.
    ##

    if echo | gcc -dM -E - | grep '__SIZEOF_LONG__ 8' ; then
        ABILINE=ABI=64
    else
        ABILINE=ABI=32
    fi

    ##
    ## Config.guess returns i386 for Mac OS, which is usually wrong.
    ##

    BUILD_LINE=
    if [ "$BUILD_OS" = "macos" ]; then
        BUILD_LINE="--build=x86_64-apple-darwin --host=x86_64-apple-darwin"
    fi

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --prefix="$OUTPUT_DIRECTORY" \
                                     --enable-static \
                                     --disable-shared \
                                     CFLAGS="$CFLAGS" \
                                     $ABILINE

    ;;

  configure)
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --enable-static \
                                     --disable-shared \
                                     --host="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

