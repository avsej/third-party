##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the xdriinfo app.
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency libgcc_6.3.0 || echo "Ignoring libgcc."
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency libx11_1.6.5
    extract_dependency glproto_1.4.17
    extract_dependency mesa_17.0.0

    extract_dependency kbproto_1.0.7
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency libxext_1.3.3

    export ac_cv_search_glXGetProcAddressARB="-lGL"
    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

