##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the groff utility.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f "$BUILD_DIRECTORY/usr/lib/charset.alias"
rm -rf "$BUILD_DIRECTORY/usr/share/man"
rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: groff
Depends: libgcc
Priority: optional
Version: 1.22.3
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/groff/groff-1.22.3.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Groff page formatting tool.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

