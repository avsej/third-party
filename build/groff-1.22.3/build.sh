##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the groff utility.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export PAGE=letter
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --with-libiconv-prefix="$DEPENDROOT/usr" \
                                     --prefix="/usr" \
                                     --with-doc=no \
                                     --disable-rpath \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    cd "$OUTPUT_DIRECTORY"/usr/bin
    ln -sfv eqn geqn
    ln -sfv tbl gtbl
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

