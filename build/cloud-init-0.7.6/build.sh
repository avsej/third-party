##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the cloud-init python package.
##
## Author:
##
##     Evan Green 26-Jan-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

SETUPTOOLS=python-setuptools_23.0.0

cd $SOURCE_DIRECTORY
case $BUILD_COMMAND in
  build)

    ##
    ## cloud-init depends on setuptools. Extract the one just built if this is
    ## a native build.
    ##

    if test "x$BUILD_OS" = "xminoca"; then
        extract_dependency "$SETUPTOOLS"
        export PATH="$DEPENDROOT/usr/bin:$PATH"
        export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib:$LD_LIBRARY_PATH"
    fi

    python setup.py build --executable="/usr/bin/python" \
                    install --init-system=sysvinit \
                            --root="$OUTPUT_DIRECTORY" \
                            --prefix=/usr \
                            --exec-prefix=/usr

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

