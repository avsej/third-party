##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the freetype library.
##
## Author:
##
##     Evan Green 7-Mar-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

HARFBUZZ=
if [ "`cat $BUILD_DIRECTORY/../harfbuzz-status`" = "yes" ] ; then
    echo "With Harfbuzz dependency"
    HARFBUZZ="libharfbuzz, "
fi

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libfreetype
Depends: bzip2, libz, libpng, $HARFBUZZ libgcc
Priority: optional
Version: 2.7.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://download.savannah.gnu.org/releases/freetype/freetype-2.7.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: FreeType font library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

