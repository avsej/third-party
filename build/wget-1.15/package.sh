##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the wget utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -Rpv "$BUILD_DIRECTORY/bin/wget" "$PACKAGE_DIRECTORY/usr/bin"
mkdir -p "$PACKAGE_DIRECTORY/etc"
cp -pv "$BUILD_DIRECTORY/etc/wgetrc" "$PACKAGE_DIRECTORY/etc"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: wget
Depends: libopenssl, libiconv, libgcc, libpcre
Priority: optional
Version: 1.15
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/wget/wget-1.15.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: wget is a utility that can fetch remote files from a web server.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/wgetrc
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

