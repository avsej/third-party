##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Tcl scripting language.
##
## Author:
##
##     Evan Green 23-May-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: tcl
Depends: libgcc, libz
Priority: optional
Version: 8.6.5
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://prdownloads.sourceforge.net/tcl/tcl8.6.5-src.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Tcl scripting language.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

