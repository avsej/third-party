##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the SDL library.
##
## Author:
##
##     Evan Green 20-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -rf $BUILD_DIRECTORY/usr/share/man
rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libsdl
Depends: libx11, libxinerama, libxrender, libxrandr, libxscrnsaver, libxcursor, libxxf86vm, mesa, libice, libgcc
Priority: optional
Version: 1.2.15
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://libsdl.org/release/SDL-1.2.15.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Simple DirectMedia Layer library (old).
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

