##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages xterm-327
##
## Author:
##
##     Evan Green 7-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/xorg/modules/drivers/*.la
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: xterm
Depends: xorg-server, libncurses, libxcursor, fontconfig, libxft, libgcc
Priority: optional
Version: 327
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://invisible-island.net/xterm/xterm-327.tgz
Installed-Size: $INSTALLED_SIZE
Description: X terminal emulator application
_EOS

mkdir -p "$PACKAGE_DIRECTORY/etc/X11/app-defaults"
cat > "$PACKAGE_DIRECTORY/etc/X11/app-defaults/XTerm" <<_EOS
XTerm*saveLines: 4096
XTerm*reverseVideo: on
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/X11/app-defaults/XTerm
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

