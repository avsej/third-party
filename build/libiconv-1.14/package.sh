##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the iconv library.
##
## Author:
##
##     Evan Green 2-Apr-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

rm "$PACKAGE_DIRECTORY"/usr/lib/*.la

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libiconv
Priority: optional
Version: 1.14
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/libiconv/libiconv-1.14.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU iconv library for converting between character sets.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

